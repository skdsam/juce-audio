<?php
/*
Plugin Name: JUCE audio keys server
Plugin URI: https://gitlab.com/skdsam/juce-audio
Description: Audio applications with serial keys
Version: 1.0
Author: Skdsam
Author URI: https://gitlab.com/skdsam/juce-audio
*/
if ( !empty($_POST["pw"]) && !empty($_POST["product"]) && !empty($_POST["mach"]) && !empty($_POST["email"])) 
{

	
    //Get our varibles
    $gb_grab = htmlspecialchars($_POST["email"]);
    $product_post = htmlspecialchars($_POST["product"]);
    $user_pw      = htmlspecialchars($_POST["pw"]);
    //$product_os = $_POST["os"]; // future use for OS related system keys 
    $product_mach = htmlspecialchars($_POST["mach"]);
	
	
    
    // Adds wordpress to the page
	if( !(include $_SERVER['DOCUMENT_ROOT'].'/wp-load.php') )
    die('<H1>We have an issue</H1>');
	
 //echo $product_post .' - '.$user_pw .' - '.$product_mach.' - '.$gb_grab;
 
    //Get user by email
    $user             = get_user_by('email', $gb_grab);
    $user_id          = $user->ID;
    //Check password is correct for the email address
    $password_checker = wp_check_password($user_pw, $user->user_pass, $user_id);
     

    if ($password_checker == 1) 
	{
    
        //Check the person owns the product
        global $wpdb;
        
        // This SQL query allows to get all the products purchased by the current user
        // In this example we sort products by date and product type
        $purchased_products_ids = $wpdb->get_col($wpdb->prepare("
        SELECT      itemmeta.meta_value
        FROM        " . $wpdb->prefix . "woocommerce_order_itemmeta itemmeta
        INNER JOIN  " . $wpdb->prefix . "woocommerce_order_items items
                    ON itemmeta.order_item_id = items.order_item_id
        INNER JOIN  $wpdb->posts orders
                    ON orders.ID = items.order_id
        INNER JOIN  $wpdb->postmeta ordermeta
                    ON orders.ID = ordermeta.post_id
        WHERE       itemmeta.meta_key = '_product_id'
                    AND ordermeta.meta_key = '_customer_user'
                    AND ordermeta.meta_value = %s
        ORDER BY    orders.post_date DESC
        ", $user_id));
        
        // Some orders may contain the same product, but we do not need it twice
        $purchased_products_ids = array_unique($purchased_products_ids);
        
        // If the customer purchased something
        if (!empty($purchased_products_ids)):
        // it is time for a regular WP_Query
            $purchased_products = new WP_Query(array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'post__in' => $purchased_products_ids,
                'orderby' => 'post__in',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_type',
                        'field' => 'slug',
                        'terms' => 'jac_audio'
                    )
                )
            ));
        //Grab our owned products from the jac_audio 
            $jac_audio_array    = array();
            while ($purchased_products->have_posts()):
                $purchased_products->the_post();
                $jac_audio_array[] = get_the_ID();
            endwhile;
            wp_reset_postdata();
        else:
        
        // respond to plugin
		
		header("Content-type: text/xml; charset=utf-8");  
        $response  = '<?xml version="1.0" encoding="utf-8"?>';
$response .= '<ERROR
    error="'.__( 'Sorry, we were not able to authorise your request. It appears you do not own this product.', 'jac-audio-default' ).'">
</ERROR>';
echo $response;



endif;



// Empty array dont do anything as we outputted our no purchase already
if (!empty($jac_audio_array))
{


foreach ($jac_audio_array as $jacaudio_product_prime)
{

// Grab our product ID
$product = wc_get_product($jacaudio_product_prime);

// Get Product General Info
$product->get_type();
$product->get_name();
$product->get_slug();
get_permalink($product->get_id());

// Get our new custom fields
$jacaudio_product_private_key_output = get_post_meta($jacaudio_product_prime, 'jacaudio_product_private_key', true);
$jacaudio_product_public_key_output = get_post_meta($jacaudio_product_prime, 'jacaudio_product_public_key', true);
$jacaudio_product_active_amount = get_post_meta($jacaudio_product_prime, 'jacaudio_product_registration_amount', true);
$jacaudio_product_name = get_post_meta($jacaudio_product_prime, 'jacaudio_product_app_name', true);



if ($product_post == $jacaudio_product_name)
{



//////////////////////////////////////////////////////////////////////////////////////
// Check the plugin database for amount of used or unsed keys
//
//////////////////////////////////////////////////////////////////////////////////////

//Table name to search
$table_name = $wpdb->prefix . 'jacaudio';

// product query against users id
$query = $wpdb->get_results("
SELECT *
FROM $table_name
WHERE user_id = $user_id
AND application_id = '$jacaudio_product_name'
", ARRAY_A);

// amount of keys found in the database to check for amount you can have
$rowcount = $wpdb->num_rows;

//Echo our reselts if we have them

if ($rowcount < $jacaudio_product_active_amount) { //Send responce to plugin header("Content-type: text/xml;
    charset=utf-8"); unlocker_sendResponse_server($jacaudio_product_name, $user_id, $gb_grab, $user_pw, $product_mach,
    $jacaudio_product_private_key_output, $jacaudio_product_public_key_output); } else { header("Content-type: text/xml;
    charset=utf-8"); $response='<?xml version="1.0" encoding="utf-8"?>' ; $response .='<ERROR error="'
    .__( 'Sorry, You have no activation keys left to use.' , 'jac-audio-default' ).'">
    </ERROR>';
    echo $response;

    }

    }

    }

    }

    }else{

    // respond to the plugin
    header("Content-type: text/xml; charset=utf-8");
    $response = '
    <?xml version="1.0" encoding="utf-8"?>';
    $response .= '<ERROR error="'.__( 'Sorry, You did not enter correct details.', 'jac-audio-default' ).'"></ERROR>';
    echo $response;

    }


    } else {}







    function unlocker_sendResponse_server($jacaudio_product_name, $user_id, $gb_grab, $user_pw, $product_mach,
    $jacaudio_product_private_key_output, $jacaudio_product_public_key_output)
    {

    $output; //run unix file and attach the key generated into the table
    $return_var; //will hold the number of lines in output string after execution
    $unlocker_url = dirname(__FILE__).'/Unlocker';


    exec(escapeshellarg($unlocker_url).' '
    .escapeshellarg($jacaudio_product_name).' '
    .escapeshellarg($gb_grab).' '
    .escapeshellarg($user_pw).' '
    .escapeshellarg($product_mach).' '
    .escapeshellarg($jacaudio_product_private_key_output.$jacaudio_product_public_key_output) //Keys private -public
    , $output, $return_var);


    $formattedOutput;

    for ($i = 5; $i < $return_var; $i++) { $formattedOutput .=$output[$i]; if ($i <=4) //everything before the key {
        $formattedOutput .="\n" ; } } // insert to database $the_date=date('Y-m-d'); global $wpdb; $table_name=$wpdb->
        prefix . 'jacaudio';
        $data = array(
        'user_id' => $user_id,
        'application_id' => $jacaudio_product_name,
        'activation_date' => $the_date,
        'machine_id' => $product_mach,
        'activation_method' => 'Online',
        'serial_number' => $formattedOutput
        );
        // $format = array('%s','%d');
        $wpdb->insert($table_name, $data);
        // $my_id = $wpdb->insert_id;


        //Send responce to plugin
        header("Content-type: text/xml; charset=utf-8");
        $response = '
        <?xml version="1.0" encoding="utf-8"?>';
        $response .= '<MESSAGE message="'.__( 'Thanks for registering our product!', 'jac-audio-default' ).'">
            <KEY>'.$formattedOutput.'</KEY>
        </MESSAGE>';
        echo $response;

        }