=== JAC ===

Requires at least: 4.2
WordPress tested up to: 5.3.2
WooCommerce tested up to: 4.0
Requires PHP: 7.0

== Description ==

WordPress, WooCommerce plugin to allow activation of JUCE audio plugins.

== Requires ==

* HTTPS - wont run without having SSL installed
* Needs a server that can run shell commands
* 

== Changelog ==

= 1.0 =
* Release Date - 21 Oct 2019*
* Fix: 
* Tweak:
* Added:
* Changed:
* Removed:


