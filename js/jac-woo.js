/////////////////////////////////////////////////////////
// Admin panel fix for general panel items
/////////////////////////////////////////////////////////
jQuery(document).ready(function($) {

    jQuery('#product-type').on('change', function() {

        var optionText = jQuery("#product-type option:selected").text();

        if (optionText == 'jac keys') {
            // Keep the general tab showing on our product
            jQuery(".general_tab").delay(800).addClass("jac-general");
            jQuery(".general_options").delay(800).addClass("jac-general");
            jQuery(".pricing").delay(800).addClass("jac-general");
            // Price settings
            jQuery(".pricing").delay(800).addClass("show_if_jacaudio_product");
            // virtual sections
            jQuery(".tips").delay(800).removeClass("show_if_simple");
            jQuery(".tips").delay(800).addClass("show_if_jacaudio_product");

        } else {

            jQuery(".tips").delay(800).addClass("show_if_simple");
            jQuery(".tips").delay(800).removeClass("show_if_jacaudio_product");

            jQuery(".pricing").delay(800).removeClass("show_if_jacaudio_product");

            jQuery(".tips").delay(800).addClass("show_if_simple");
            jQuery(".tips").delay(800).removeClass("show_if_jacaudio_product");
        }
    });

    // Validate our fields to stop them being empty
    /////////////////////////////////////////////////////////

    jQuery("#publish").click(function(event) {

        var optionText = jQuery("#product-type option:selected").text();

        if (optionText == 'jac keys') {

            if (jQuery('#jacaudio_product_registration_amount').val() == '') {
                event.preventDefault();
                var infoprom = prompt("Please enter activation count", "");
                if (infoprom != null) {
                    jQuery('#jacaudio_product_registration_amount').val(infoprom);
                }
            }
            if ($('#jacaudio_product_public_key').val() == '') {
                event.preventDefault();
                var infoprom = prompt("Please enter your public key", "");
                if (infoprom != null) {
                    jQuery('#jacaudio_product_public_key').val(infoprom);
                }
            }
            if (jQuery('#jacaudio_product_private_key').val() == '') {
                event.preventDefault();
                var infoprom = prompt("Please enter your private key", "");
                if (infoprom != null) {
                    jQuery('#jacaudio_product_private_key').val(infoprom);
                }
            }
            if ($('#jacaudio_product_app_name').val() == '') {
                event.preventDefault();
                var infoprom = prompt("Please enter your application name", "");
                if (infoprom != null) {
                    jQuery('#jacaudio_product_app_name').val(infoprom);
                }
            }
        }


    });

    // Keys not checked below as might be removed 
    // jacaudio_product_private_key_delete jacaudio_product_public_key_delete

});