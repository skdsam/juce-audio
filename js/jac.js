/////////////////////////////////////////////////////////
// Stop forms default actions
/////////////////////////////////////////////////////////
jQuery(document).ready(function($) {

    // Stop forms running
    jQuery("#jac-deauth-form").submit(function(e) {
        return false;
    });
    jQuery("#jac-master-deauth-form").submit(function(e) {
        return false;
    });
    jQuery("#jac-create").submit(function(e) {
        return false;
    });

    /////////////////////////////////////////////////////////
    // Master keys confirmation box
    /////////////////////////////////////////////////////////
    jQuery('.master_deauther_button_panel').click(function() {



        // Grab our table id number
        var t = this.id;
        t = t.replace('master_deauther_button_', '');
        // Assign to the ide to a button    jac-confirm-deauth
        jQuery('.master_deauther_button_con').attr("id", t);


    });

    jQuery('.master_deauther_button_con').click(function() {



        // Grab our table id number
        t = document.querySelector('.master_deauther_button_con').id;

        var data = {
            action: 'jac_deathaction_master',
            security: MyAjax.security,
            authinfo: t
        };

        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        $.post(MyAjax.ajaxurl, data, function(response) {
            if (response == "Deautherised plugin") {

                jQuery("#masterModalDe").modal("hide");
                jQuery.toast({
                    heading: 'Success',
                    text: 'Application was deautherised!',
                    showHideTransition: 'slide',
                    icon: 'success'
                });

                //Reload page
                setTimeout(function() { // wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 2000);

            }
            if (response == "An error occured please try again later!") {

                jQuery("#masterModalDe").modal("hide");
                jQuery.toast({
                    heading: 'Error',
                    text: 'An error occured please try again later!',
                    showHideTransition: 'fade',
                    icon: 'error'
                });

                //Reload page
                setTimeout(function() { // wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 2000);

            }

        });
    });



    /////////////////////////////////////////////////////////
    // Ajax deautherise plugin key
    /////////////////////////////////////////////////////////



    jQuery('.jac-deauth').click(function() {



        // Grab our table id number
        var t = this.id;
        t = t.replace('deauther_button_', '');
        // Assign to the ide to a button    jac-confirm-deauth
        jQuery('.jac-confirm-deauth').attr("id", t);


    });


    jQuery('.jac-confirm-deauth').click(function() {




        var tk = this.id;

        var data = {
            action: 'jac_deathaction',
            security: MyAjax.security,
            authinfo: tk
        };

        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        $.post(MyAjax.ajaxurl, data, function(response) {
            if (response == "Deautherised plugin") {

                jQuery("#myModalDe").modal("hide");


                jQuery.toast({
                    heading: 'Success',
                    text: 'Application was deautherised!',
                    showHideTransition: 'slide',
                    icon: 'success'
                });

                //Reload page
                setTimeout(function() { // wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 2000);

            }
            if (response == "Nothing found") {
                jQuery("#myModalDe").modal("hide");

                jQuery.toast({
                    heading: 'Warning',
                    text: 'Something went wrong, please try again.',
                    showHideTransition: 'plain',
                    icon: 'warning'
                });

                //Reload page
                setTimeout(function() { // wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 2000);

            }
            if (response == "An error occured please try again later!") {
                jQuery("#myModalDe").modal("hide");


                jQuery.toast({
                    heading: 'Error',
                    text: 'An error occured please try again later!',
                    showHideTransition: 'fade',
                    icon: 'error'
                });

                //Reload page
                setTimeout(function() { // wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 2000);
            }

        });

    });




    /////////////////////////////////////////////////////////
    // Ajax offline auth model fix
    /////////////////////////////////////////////////////////


    // Grabs the product name to add to the form
    jQuery('#aoModal').on('show.bs.modal', function(event) {

        var button = jQuery(event.relatedTarget); // Button that triggered the modal
        data_app = button.attr("data-app");
        jQuery("#applicationname").val(data_app);

    });




    jQuery('#jac-confirm-ao').click(function() {

        // check input fields
        application_name = jQuery("#applicationname").val();
        password_name = jQuery("#password_auth").val();
        machine_name = jQuery("#machine_auth").val();

        if (jQuery.trim(jQuery("#applicationname").val()) != "" && jQuery.trim(jQuery("#machine_auth").val()) != "" && jQuery.trim(jQuery("#password_auth").val()) != "") {


            var data = {
                action: 'jac_athaction',
                security: MyAjax.security,
                'password_a': password_name,
                'machine_a': machine_name,
                'application_a': application_name

            };

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            $.post(MyAjax.ajaxurl, data, function(response) {


                if (response == "offline actioned") {

                    jQuery("#aoModal").modal("hide");

                    jQuery.toast({
                        heading: 'Success',
                        text: 'Application was deautherised!',
                        showHideTransition: 'slide',
                        icon: 'success'
                    });

                    //Reload page
                    setTimeout(function() { // wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 2000);

                }
                if (response == "error password") {
                    jQuery("#aoModal").modal("hide");

                    jQuery.toast({
                        heading: 'Warning',
                        text: 'The wrong password was entered.',
                        showHideTransition: 'plain',
                        icon: 'warning'
                    });

                    //Reload page

                }
                if (response == "error products") {
                    jQuery("#aoModal").modal("hide");

                    jQuery.toast({
                        heading: 'Error',
                        text: 'An error occured please try again later!',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });


                    //Reload page
                    setTimeout(function() { // wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 2000);


                }

            });




        } else {}
    });




    // Close ready
    /////////////////////////////////////////////////////////

});



/////////////////////////////////////////////////////////
// Filter for the master tabel panel in the admin page
/////////////////////////////////////////////////////////
jQuery(document).ready(function() {

    jQuery("#searchitems").on("keyup", function() {
        var value = jQuery(this).val().toLowerCase();
        jQuery("#master-key-delete tr").filter(function() {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});


/////////////////////////////////////////////////////////
// Popover for modal
/////////////////////////////////////////////////////////
jQuery(document).ready(function() {

    jQuery('[data-toggle="popover"]').popover();

});

/////////////////////////////////////////////////////////
// Copy to clipboard
/////////////////////////////////////////////////////////


function copyToClipboard(text, el) {
    var copyTest = document.queryCommandSupported('copy');
    var elOriginalText = el.attr('data-original-title');

    if (copyTest === true) {
        var copyTextArea = document.createElement("textarea");
        copyTextArea.value = text;
        document.body.appendChild(copyTextArea);
        copyTextArea.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'Copied!' : 'Whoops, not copied!';
            el.attr('data-original-title', msg).tooltip('show');
        } catch (err) {
            console.log('Oops, unable to copy');
        }
        document.body.removeChild(copyTextArea);
        el.attr('data-original-title', elOriginalText);
    } else {
        // Fallback if browser doesn't support .execCommand('copy')
        window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
    }
}

jQuery(document).ready(function() {
    // Initialize
    // ---------------------------------------------------------------------

    // Tooltips
    // Requires Bootstrap 3 for functionality
    jQuery('.js-tooltip').tooltip();

    // Copy to clipboard
    // Grab any text in the attribute 'data-copy' and pass it to the 
    // copy function
    jQuery('.js-copy').click(function() {
        var text = jQuery(this).attr('data-copy');
        var el = jQuery(this);
        copyToClipboard(text, el);
    });
});