<?php
/*
Plugin Name: JUCE audio keys
Plugin URI: https://gitlab.com/skdsam/juce-audio
Description: Audio applications with serial keys
Version: 1.0
Author: Skdsam
Author URI: https://gitlab.com/skdsam/juce-audio
*/


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Add extra links to plugin page
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'jac_plugin_action_links' );

function jac_plugin_action_links( $links ) {
   $links[] = '<a href="'. esc_url( get_admin_url(null, 'options-general.php?page=jac-audio') ) .'">Settings</a>';
   return $links;
}

add_filter( 'plugin_row_meta', 'misha_support_and_faq_links', 10, 4 );
 
function misha_support_and_faq_links( $links_array, $plugin_file_name, $plugin_data, $status ){
 
	if(  $plugin_file_name == plugin_basename(__FILE__) ) {
		// you can still use array_unshift() to add links at the beginning
		//$links_array[] = '<a href="#">FAQ</a>';
		//$links_array[] = '<a href="#">Support</a>';
		//$links_array[] = '<a href="#">Check for updates</a>';
	}
 
	return $links_array;
}


//Inform needs WooCommerce on plugin info panel
add_action( 'after_plugin_row_' . plugin_basename( __FILE__ ), function() 
{ 
?>
<tr class="active">
    <th class="check-column"></th>
    <td></td>
    <td>
        <div class="notice inline notice-warning notice-alt">
            <p><?php _e( 'This plugin requires. WooCommerce to be installed and active for its features to work.', 'jac-audio-default' ); ?>
            </p>
        </div>
        <?php
 if (!is_ssl()) {
?><div class="notice inline notice-error notice-alt">
            <p><?php _e( 'You must setup SSL for this plugin to work.', 'jac-audio-default' ); ?></p>
        </div>
        <?php     
 }
?>


    </td>
</tr>
<?php 
} );



// Stops errors to site if WooCommerce is not installed and active
$all_plugins = apply_filters('active_plugins', get_option('active_plugins'));
if (stripos(implode($all_plugins), 'woocommerce.php')) {
 


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Add our CSS and Javascript files
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function jacaudio_style($hook)
{

  global $post_type;
  global $wp_query;

    if( 'product' == $post_type )
    {
     wp_enqueue_script('jac-admin-woo-js', plugin_dir_url(__FILE__) . 'js/jac-woo.js', array('jquery'), '1.0', false);
     wp_enqueue_style('jac-css', plugins_url('css/style.css', __FILE__));
    }
	
    if( $hook == 'toplevel_page_jac-audio' || isset( $wp_query->query_vars['jac-support']) ) 
    {
  	  wp_enqueue_style('bootstrap', plugins_url('css/bootstrap.css', __FILE__),'', '3.4.0', false);
      wp_enqueue_style('toast-css', plugins_url('css/jquery.toast.css', __FILE__),'', '', false);	
      wp_enqueue_style('jac-css', plugins_url('css/style.css', __FILE__));		
      wp_enqueue_script('jquery', plugin_dir_url(__FILE__) . 'js/jquery.min.js', false, '3.4.0', true); 
  	  wp_enqueue_script('popper-js', plugin_dir_url(__FILE__) . 'js/popper.min.js', array('jquery'), '3.4.0', true);
      wp_enqueue_script('bootstrap-js', plugin_dir_url(__FILE__) . 'js/bootstrap.js', array('jquery'), '4.3.1', true);
	  wp_enqueue_script('jac-js', plugin_dir_url(__FILE__) . 'js/jquery.toast.js', array('jquery'), '', true);
	  wp_enqueue_script('toast-js', plugin_dir_url(__FILE__) . 'js/jac.js', array('jquery'), '1.0', true);
	
	wp_localize_script( 'jac-js', 'MyAjax', array(
    // URL to wp-admin/admin-ajax.php to process the request
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
    // generate a nonce with a unique ID "myajax-post-comment-nonce"
    // so that you can check it later when an AJAX request is sent
    'security' => wp_create_nonce( 'my-special-string' )
    ));
    
	}

    
	
}
add_action('wp_enqueue_scripts', 'jacaudio_style');
add_action('admin_enqueue_scripts', 'jacaudio_style');



////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STARTUP AND ACTIVATIONS - DEACTIVATIONS OF PLUGIN
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/**
 * activation  of plugin
 */
function jac_activate()
{
 

}
register_activation_hook(__FILE__, 'jac_activate');




/**
 * Actions to be done when plugin deactivated
 */
function jac_deactivate_plugin()
{
    
}
register_deactivation_hook(__FILE__, 'jac_deactivate_plugin');


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ddc our global variables
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function jak_load_plugin()
{
	
add_option('jacaudio_plugin_version', '0.2'); // Define plugin version
add_option('jacaudio_plugin_auth_folder', 'auth'); // Define plugin deauth url

///FIX THIS LATER TO INCLUDE THE NEW FILE PATH
add_option('jacaudio_plugin_auth', plugins_url( '/auth/auth.php', __FILE__ )); // Define plugin auth url
add_option('jacaudio_plugin_deauth', plugins_url( '/auth/deauth.php', __FILE__ )); // Define plugin deauth url

register_setting( 'jac_options_page_group', 'jacaudio_plugin_auth_folder', 'jac_options_callback' );

}
add_action('admin_init', 'jak_load_plugin');


function jac_options_callback2($input){
	
	
// Old Name Of The file 
$old_name = plugins_url( '/', __FILE__ ).get_option('jacaudio_plugin_auth_folder'); 
// New Name For The File 
$new_name = plugins_url( '/', __FILE__ ).update_option( 'jacaudio_plugin_auth_folder', $input );
   
// Checking If File Already Exists 
 if(file_exists(plugins_url( '/', __FILE__ ).$old_name)) 
 {  
    
 rename( $old_name, $new_name);
  
 }
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Database setup 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

global $jac_db_version;
$jac_db_version = '2.0';

function jac_install()
{
    global $wpdb;
    global $jac_db_version;
    
    $table_name = $wpdb->prefix . 'jacaudio';
    
    
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        
        $charset_collate = $wpdb->get_charset_collate();
        
        $sql = "CREATE TABLE $table_name (
		id mediumint(11) NOT NULL AUTO_INCREMENT,
		user_id int(11) NOT NULL,
		product_post_id int(11) NOT NULL,
		application_id text NOT NULL,
		activation_date date DEFAULT '00-00-0000' NOT NULL,
		machine_id text NOT NULL,
		activation_method text(12) NOT NULL,
		serial_number text,
        operating_system text,
		PRIMARY KEY  (id)
	) $charset_collate;";
        
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        
    }
    add_option('jac_db_version', $jac_db_version);
}


global $wpdb;
$installed_ver = get_option( "jac_db_version" );

if ( $installed_ver != $jac_db_version ) {

$table_name = $wpdb->prefix . 'jacaudio';

 $sql = "CREATE TABLE $table_name (
		id mediumint(11) NOT NULL AUTO_INCREMENT,
		user_id int(11) NOT NULL,
		product_post_id int(11) NOT NULL,
		application_id text NOT NULL,
		activation_date date DEFAULT '00-00-0000' NOT NULL,
		machine_id text NOT NULL,
		activation_method text(12) NOT NULL,
		serial_number text,
        operating_system text,
		PRIMARY KEY  (id)
	)";

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql );

update_option( "jac_db_version", $jac_db_version );
}



function jac_update_db_check() {
	
   global $jac_db_version;
   if ( get_site_option( 'jac_db_version' ) != $jac_db_version ) 
   {
     jac_install();
   }
}
add_action( 'plugins_loaded', 'jac_update_db_check' );


register_activation_hook(__FILE__, 'jac_install');


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Menu pages
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


add_action('admin_menu', 'jacaudio_menu');

function jacaudio_menu()
{
    
    $page_title = 'jac audio';
    $menu_title = 'JAC AUDIO';
    $capability = 'manage_options';
    $menu_slug  = 'jac-audio';
    $function   = 'jacaudio_page';
    $icon_url   = 'dashicons-controls-volumeon';
    $position   = 4;
    
    add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position);
}




function jacaudio_page()
{

if ( is_admin() ) 
{
 
?>


<div class="container mt-3">

    <br>


    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#home"><?php _e( 'Home', 'jac-audio-default' ); ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#menu1"><?php _e( 'Settings', 'jac-audio-default' ); ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#menu2"><?php _e( 'Help', 'jac-audio-default' ); ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#menu3"><?php _e( 'Revoke Keys', 'jac-audio-default' ); ?></a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div id="home" class="container tab-pane active"><br>

            <!-- Section: Features v.3 -->
            <section class="my-5">

                <!-- Section heading -->
                <h2 class="h1-responsive font-weight-bold text-center my-5">
                    <?php _e( 'Version: 1.0 Whats new?', 'jac-audio-default' ); ?></h2>
                <!-- Section description -->
                <p class="lead grey-text text-center w-responsive mx-auto mb-5">
                    <?php _e( 'JAC audio plugin allowing you to easily license and deploy products within your own online store.', 'jac-audio-default' ); ?>
                </p>

                <!-- Grid row -->
                <div class="row">

                    <!-- Grid column -->
                    <div class="col-lg-5 text-center text-lg-left">
                        <img class="img-fluid" src="<?php echo plugins_url( '/images/info.png', __FILE__ ); ?>"
                            alt="Sample image">
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-lg-7">

                        <!-- Grid row -->
                        <div class="row mb-3">

                            <!-- Grid column -->
                            <div class="col-1">
                                <i class="fas fa-share fa-lg indigo-text"></i>
                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-xl-10 col-md-11 col-10">
                                <h5 class="font-weight-bold mb-3">
                                    <?php _e( 'Offline Authorising', 'jac-audio-default' ); ?></h5>
                                <p class="grey-text">
                                    <?php _e( 'You are now able to authorise your products via our offline method to allow users to implement the plugin on any system without internet connection.', 'jac-audio-default' ); ?>
                                </p>
                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- Grid row -->

                        <!-- Grid row -->
                        <div class="row mb-3">

                            <!-- Grid column -->
                            <div class="col-1">
                                <i class="fas fa-share fa-lg indigo-text"></i>
                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-xl-10 col-md-11 col-10">
                                <h5 class="font-weight-bold mb-3"><?php _e( 'Pure JUCE', 'jac-audio-default' ); ?></h5>
                                <p class="grey-text">
                                    <?php _e( 'We have taken the time to implement the JUCE system codes into easy to manage files that takes care of the plugin activations, so you can concentrate on building your plugin.', 'jac-audio-default' ); ?>
                                </p>
                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- Grid row -->

                        <!--Grid row-->
                        <div class="row">

                            <!-- Grid column -->
                            <div class="col-1">
                                <i class="fas fa-share fa-lg indigo-text"></i>
                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-xl-10 col-md-11 col-10">
                                <h5 class="font-weight-bold mb-3">
                                    <?php _e( 'Custom Product Type', 'jac-audio-default' ); ?></h5>
                                <p class="grey-text mb-0">
                                    <?php _e( 'Custom product type has been created to allow you to simply add your audio products and all the key details without having to leave the create product page. ', 'jac-audio-default' ); ?>
                                </p>
                            </div>
                            <!-- Grid column -->

                        </div>
                        <!--Grid row-->

                    </div>
                    <!--Grid column-->

                </div>
                <!-- Grid row -->

            </section>
            <!-- Section: Features v.3 -->





            <!-- Section: -->
            <div class="container cus1"><br>
                <p><?php _e( 'For a full list of changes please view our change log.', 'jac-audio-default' );  ?> <a
                        href="<?php echo plugins_url( '/readme.txt', __FILE__ ); ?>"><?php _e( 'Change Log', 'jac-audio-default' );  ?>
                    </a></p>



            </div>



        </div>



        <div id="menu1" class="container tab-pane fade"><br>

            <div class="container">

                <div class="card col-md-12">
                    <div class="card-header">
                        <h4><?php _e( 'Settings', 'jac-audio-default' ); ?></h4>
                    </div>
                    <div class="card-body">

                        <form method="post" action="options.php">
                            <?php settings_fields( 'jac_options_page_group' ); ?>
                            <div class="form-group" method="post" action="options.php">
                                <label
                                    for="jacaudio_plugin_auth_folder"><?php _e( 'Set your default name for the authentication folder', 'jac-audio-default' );  ?></label>
                                <input type="text" class="form-control form-control-sm" id="jacaudio_plugin_auth_folder"
                                    name="jacaudio_plugin_auth_folder"
                                    value="<?php echo get_option('jacaudio_plugin_auth_folder'); ?>" />
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </form>

                    </div>
                </div>


                <br />
            </div>


            <div class="container">

                <div class="card col-md-12">
                    <div class="card-header">
                        <h4><?php _e( 'Audio application links for autherisations', 'jac-audio-default' ); ?></h4>
                    </div>
                    <div class="card-body">


                        <div class="my-3 p-3 bg-white rounded box-shadow">
                            <div class="media text-muted pt-3">
                                <button type="button"
                                    class="btn btn-default btn-copy js-tooltip js-copy btn-elegant float-right btn-sm"
                                    data-toggle="tooltip" data-placement="bottom"
                                    data-copy="<?php  echo get_option( 'jacaudio_plugin_auth' ); ?>"
                                    title="Copy to clipboard"><?php _e( 'Copy', 'jac-audio-default' ); ?></button>
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong
                                        class="d-block text-gray-dark"><?php _e( 'Plugin Authorise link', 'jac-audio-default' ); ?></strong>
                                    <?php  echo get_option( 'jacaudio_plugin_auth' ); ?>
                                </p>
                            </div>

                            <div class="media text-muted pt-3">
                                <button type="button"
                                    class="btn btn-default btn-copy js-tooltip js-copy btn-elegant float-right btn-sm"
                                    data-toggle="tooltip" data-placement="bottom"
                                    data-copy="<?php  echo get_option( 'jacaudio_plugin_deauth' ); ?>"
                                    title="Copy to clipboard"><?php _e( 'Copy', 'jac-audio-default' ); ?></button>
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong
                                        class="d-block text-gray-dark"><?php _e( 'Plugin Deauthorise link', 'jac-audio-default' ); ?></strong>
                                    <?php  echo get_option( 'jacaudio_plugin_deauth' ); ?>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>

                <br />
            </div>

            <div class="container">
                <h4> <?php _e( 'Plugin info', 'jac-audio-default' ); ?></h4>
                <ul class="list-group">

                    <?php
 
  if (get_option( 'jacaudio_plugin_auth_folder' ) == 'auth') {
   
?>
                    <div class="jac-notice jac-info">
                        <p><?php _e( 'This plugin will work but would add an addtional layer of security to change the default folder that the autherisation files are in.', 'jac-audio-default' ); ?>
                        </p>
                    </div>
                    <?php	
  }
  else
  {
?>
                    <div class="jac-notice jac-success">
                        <p><?php _e( 'You changed the default folder location, this is the best choice to add extra security.', 'jac-audio-default' ); ?>
                        </p>
                    </div>
                    <?php	  
  }		 
 
  if (is_ssl()) {
   
?>
                    <div class="jac-notice jac-success">
                        <p><?php _e( 'SSL is active', 'jac-audio-default' ); ?></p>
                    </div>
                    <?php	
  }
  else{
?>
                    <div class="jac-notice jac-error">
                        <p><?php _e( 'SSL is not active', 'jac-audio-default' ); ?></p>
                    </div>
                    <?php	  
  }

if (function_exists('exec')) {
   
?>
                    <div class="jac-notice jac-success">
                        <p><?php _e( 'Exec is active', 'jac-audio-default' ); ?></p>
                    </div>
                    <?php	
  }
  else{
?>
                    <div class="jac-notice jac-error">
                        <p><?php _e( 'Exec is not active', 'jac-audio-default' ); ?></p>
                    </div>
                    <?php	  
  }


// Check our unlocker is 777 if not change it
$check_unlocker_perms_value = substr(sprintf('%o', fileperms(__DIR__.'/auth/Unlocker')), -4);
// try set to 0777 auto
chmod(__DIR__.'/auth/Unlocker',0777);

$check_keygen_perms_value = substr(sprintf('%o', fileperms(__DIR__.'/auth/KeyGenerator')), -4);
// try set to 0777 auto
chmod(__DIR__.'/auth/KeyGenerator',0777);



  if ( $check_unlocker_perms_value == '777' ) {
   
?>
                    <div class="jac-notice jac-success">
                        <p><?php _e( 'Your Unlocker file has the correct permissions', 'jac-audio-default' ); ?></p>
                    </div>
                    <?php	
  }
  else{
?>
                    <div class="jac-notice jac-error">
                        <p><?php _e( 'Your Unlocker file should have the permission of 0777 but has the wrong permissions and will not generate keys ', 'jac-audio-default' ); ?>(<?php  echo $check_unlocker_perms_value; ?>)
                        </p>
                    </div>
                    <?php	  
  } 


if ( $check_keygen_perms_value == '777' ) {
   
?>
                    <div class="jac-notice jac-success">
                        <p><?php _e( 'Your keygen file has the correct permissions', 'jac-audio-default' ); ?></p>
                    </div>
                    <?php	
  }
  else{
?>
                    <div class="jac-notice jac-error">
                        <p><?php _e( 'Your keygen file should have the permission of 0777 but has the wrong permissions and will not generate random RSA keys ', 'jac-audio-default' ); ?>(<?php  echo $check_unlocker_perms_value; ?>)
                        </p>
                    </div>
                    <?php	  
  } 


?>
                </ul>
            </div>


        </div>


        <div id="menu2" class="container tab-pane fade"><br>

            <!--Carousel Wrapper-->
            <div id="video-carousel-example2" class="carousel slide carousel-fade" data-ride="carousel">
                <!--Indicators-->
                <ol class="carousel-indicators">
                    <li data-target="#video-carousel-example2" data-slide-to="0" class="active"></li>
                </ol>
                <!--/.Indicators-->
                <!--Slides-->
                <div class="carousel-inner" role="listbox">
                    <!-- First slide -->
                    <div class="carousel-item active">
                        <!--Mask color-->
                        <div class="view">
                            <!--Video source-->
                            <video class="video-fluid" autoplay loop muted>
                                <source src="https://mdbootstrap.com/img/video/Lines.mp4" type="video/mp4" />
                            </video>
                            <div class="mask rgba-indigo-light"></div>
                        </div>

                        <!--Caption-->
                        <div class="carousel-caption">
                            <div class="animated fadeInDown">
                                <h3 class="h3-responsive"><?php _e( 'Help Video 1', 'jac-audio-default' ); ?></h3>
                            </div>
                        </div>
                        <!--Caption-->
                    </div>
                    <!-- /.First slide -->



                </div>
                <!--/.Slides-->
                <!--Controls-->
                <a class="carousel-control-prev" href="#video-carousel-example2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only"><?php _e( 'Previous', 'jac-audio-default' ); ?></span>
                </a>
                <a class="carousel-control-next" href="#video-carousel-example2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only"><?php _e( 'Next', 'jac-audio-default' ); ?></span>
                </a>
                <!--/.Controls-->
            </div>
            <!--Carousel Wrapper-->

        </div>

        <div id="menu3" class="container tab-pane fade"><br>



            <div class="container">
                <h4><?php _e( 'Remove keys from users', 'jac-audio-default' ); ?></h4>

                <?php
if ( is_admin() ) 
{
 
echo do_shortcode("[jac-remove-keys-admin]");
}
?>

            </div>


        </div>


    </div>

    <!-- The Modal deauth -->
    <div class="modal fade" id="masterModalDe">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">
                        <?php _e( 'Are you sure you want to deautherise this plugin?', 'jac-audio-default' ); ?></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <form id="jac-deauth-form">

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" id="" class="master_deauther_button_con btn btn-primary"
                                    onclick="#"><?php _e( 'Deautherise', 'jac-audio-default' ); ?></button>
                                <p id="jac-status"></p>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger"
                        data-dismiss="modal"><?php _e( 'Close', 'jac-audio-default' ); ?></button>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal end -->

</div>


<?php
}

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WooCommerce add-ons section
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Register a custom woocommrece panel tab area.
 // Note: Resave Permalinks or it will give 404 error - try to fix this with a one off warning
 **/


function jac_add_premium_support_endpoint()
{
    add_rewrite_endpoint('jac-support', EP_ROOT | EP_PAGES);
}
add_action('init', 'jac_add_premium_support_endpoint');

function jac_premium_support_query_vars($vars)
{
    $vars[] = 'jac-support';
    return $vars;
}
add_filter('query_vars', 'jac_premium_support_query_vars', 0);

function jac_add_premium_support_link_my_account($items)
{
    $items['jac-support'] = 'Authorisations';
    return $items;
}
add_filter('woocommerce_account_menu_items', 'jac_add_premium_support_link_my_account');



/**
 * Display our shortcode inside the woo tab
 // 
 **/
function jac_premium_support_content()
{
      
    // Display our shortcode
    echo do_shortcode("[jacaudio_product_short]");
}
add_action('woocommerce_account_jac-support_endpoint', 'jac_premium_support_content');


/**
 * Create our audio product type
 // 
 **/

// Register the new product type
add_action('init', 'register_jac_audio_product_type');

function register_jac_audio_product_type()
{
    
    class WC_Product_jac_audio extends WC_Product
    {
        
        public function __construct($product)
        {
            $this->product_type = 'jac_audio';
            $this->virtual      = 'yes';
            $this->downloadable = 'yes';
            $this->manage_stock = 'no';
            $this->supports[]   = 'ajax_add_to_cart';

            parent::__construct($product);
        }
    }
}



// Add to the selection of product type
add_filter('product_type_selector', 'add_jac_audio_product_type');
function add_jac_audio_product_type($types)
{
    $types['jac_audio'] = __('jac keys', 'jacaudio_product');
    return $types;

}
// Create custom audio tab in products creation
add_filter('woocommerce_product_data_tabs', 'jacaudio_product_tab');
function jacaudio_product_tab($tabs)
{
    
    $tabs['jac_audio'] = array(
        'label' => __('Jac Audio', 'jacaudio_product'),
        'target' => 'jacaudio_product_options',
        'class' => 'show_if_jacaudio_product'
    );
    return $tabs;

}

// Create custom fields for audio type
add_action('woocommerce_product_data_panels', 'jacaudio_product_tab_product_tab_content');
function jacaudio_product_tab_product_tab_content()
{

?><div id='jacaudio_product_options' class='panel woocommerce_options_panel'><?php
?><div class='options_group'>


        <?php
    
    woocommerce_wp_text_input(array(
        'id' => 'jacaudio_product_private_key',
        'label' => __('Private Key Activation', 'jacaudio_product_private_key'),
        'placeholder' => '',
        'desc_tip' => 'true',
        'description' => __('Enter Private Key.', 'jacaudio_product_private_key'),
        'type' => 'text'
    ));
    
    woocommerce_wp_text_input(array(
        'id' => 'jacaudio_product_public_key',
        'label' => __('Public Key Activation', 'jacaudio_product_public_key'),
        'placeholder' => '',
        'desc_tip' => 'true',
        'description' => __('Enter Public key.', 'jacaudio_product_public_key'),
        'type' => 'text'
    ));
    
    woocommerce_wp_text_input(array(
        'id' => 'jacaudio_product_registration_amount',
        'label' => __('Activation Count', 'jacaudio_product_registration_amount'),
        'placeholder' => '1',
        'desc_tip' => 'true',
        'description' => __('How many machines audio application can be installed on.', 'jacaudio_product_registration_amount'),
        'type' => 'number',
        'custom_attributes' => array(
            'step' => 'any',
            'min' => '0')
    ));
    
    woocommerce_wp_text_input(array(
        'id' => 'jacaudio_product_private_key_delete',
        'label' => __('Private Key Deactivation', 'jacaudio_product_private_key_delete'),
        'placeholder' => '',
        'desc_tip' => 'true',
        'description' => __('Enter Private Key for deactivation.', 'jacaudio_product_private_key_delete'),
        'type' => 'text'
    ));
    
    woocommerce_wp_text_input(array(
        'id' => 'jacaudio_product_public_key_delete',
        'label' => __('Public Key Deactivation', 'jacaudio_product_public_key_delete'),
        'placeholder' => '',
        'desc_tip' => 'true',
        'description' => __('Enter Public key for deactivation.', 'jacaudio_product_public_key_delete'),
        'type' => 'text'
    ));
	
	woocommerce_wp_text_input(array(
        'id' => 'jacaudio_product_app_name',
        'label' => __('App Name used in audio application', 'jacaudio_product_app_name'),
        'placeholder' => '',
        'desc_tip' => 'true',
        'description' => __('Enter the App name used in the application.', 'jacaudio_product_app_name'),
        'type' => 'text'
    ));
    
?></div>
</div>
<?php
    
}




// Save the details in custom fields
add_action('woocommerce_process_product_meta', 'save_jacaudio_product_settings');

function save_jacaudio_product_settings($post_id)
{
    
    $jacaudio_product_private_key         = $_POST['jacaudio_product_private_key'];
    $jacaudio_product_public_key          = $_POST['jacaudio_product_public_key'];
    $jacaudio_product_registration_amount = $_POST['jacaudio_product_registration_amount'];
    $jacaudio_product_private_key_delete  = $_POST['jacaudio_product_private_key_delete'];
    $jacaudio_product_public_key_delete   = $_POST['jacaudio_product_public_key_delete'];
  	$jacaudio_product_app_name            = $_POST['jacaudio_product_app_name'];
    
    if (!empty($jacaudio_product_private_key)) {
        
        update_post_meta($post_id, 'jacaudio_product_private_key', esc_attr($jacaudio_product_private_key));
    }
    if (!empty($jacaudio_product_public_key)) {
        
        update_post_meta($post_id, 'jacaudio_product_public_key', esc_attr($jacaudio_product_public_key));
    }
    if (!empty($jacaudio_product_registration_amount)) {
        
        update_post_meta($post_id, 'jacaudio_product_registration_amount', esc_attr($jacaudio_product_registration_amount));
    }
    if (!empty($jacaudio_product_private_key_delete)) {
        
        update_post_meta($post_id, 'jacaudio_product_private_key_delete', esc_attr($jacaudio_product_private_key_delete));
    }
    if (!empty($jacaudio_product_public_key_delete)) {
        
        update_post_meta($post_id, 'jacaudio_product_public_key_delete', esc_attr($jacaudio_product_public_key_delete));
    }
	if (!empty($jacaudio_product_app_name)) {
        
        update_post_meta($post_id, 'jacaudio_product_app_name', esc_attr($jacaudio_product_app_name));
    }
}

// Display the custom details
add_action('woocommerce_single_product_summary', 'jacaudio_product_front');

function jacaudio_product_front()
{
    global $product;
    
    if ('jac_audio' == $product->get_type()) {
         _e( 'Activation amount: ', 'jac-audio-default' );
        echo( get_post_meta( $product->get_id(), 'jacaudio_product_registration_amount' )[0] );
        echo "<br /> ";
    }
}

// Add to cart button
function jacaudio_product_cart()
{
    wc_get_template('single-product/add-to-cart/simple.php');
}
add_action('woocommerce_jac_audio_add_to_cart', 'jacaudio_product_cart');


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WooCommerce registration panel 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/**
 * Create our audio product type
 // 
 **/

function jacaudio_product_shortcode()
{
    
    global $wpdb;
    
    // This SQL query allows to get all the products purchased by the current user
    // In this example we sort products by date and product type
    $purchased_products_ids = $wpdb->get_col($wpdb->prepare("
		SELECT      itemmeta.meta_value
		FROM        " . $wpdb->prefix . "woocommerce_order_itemmeta itemmeta
		INNER JOIN  " . $wpdb->prefix . "woocommerce_order_items items
		            ON itemmeta.order_item_id = items.order_item_id
		INNER JOIN  $wpdb->posts orders
		            ON orders.ID = items.order_id
		INNER JOIN  $wpdb->postmeta ordermeta
		            ON orders.ID = ordermeta.post_id
		WHERE       itemmeta.meta_key = '_product_id'
		            AND ordermeta.meta_key = '_customer_user'
		            AND ordermeta.meta_value = %s
		ORDER BY    orders.post_date DESC
		", get_current_user_id()));
    
    // Some orders may contain the same product, but we do not need it twice
    $purchased_products_ids = array_unique($purchased_products_ids);
    
    // If the customer purchased something
    if (!empty($purchased_products_ids)):
    // it is time for a regular WP_Query
        $purchased_products = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'post__in' => $purchased_products_ids,
            'orderby' => 'post__in',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_type',
                    'field' => 'slug',
                    'terms' => 'jac_audio'
                )
            )
        ));
    //Grab our owned products from the jac_audio 
        $jac_audio_array = array();
        while ($purchased_products->have_posts()):
            $purchased_products->the_post();
            $jac_audio_array[] = get_the_ID();
        endwhile;
        wp_reset_postdata();
    else:
	_e( 'Once you purchase an item it will be displayed here.', 'jac-audio-default' );

    endif;
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////*
    //
    //  Display the products and activations panels
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////*
    
    
    // Empty array dont do anything as we outputted our no purchase already	
    if (!empty($jac_audio_array)) {
        
        //$percentage = ( 1 / 2 ) * 100;
        
?>

<?php
        
        foreach ($jac_audio_array as $jacaudio_product_prime) {
            
            // Grab our product ID
            $product = wc_get_product($jacaudio_product_prime);
            
            // Get Product General Info
            $product->get_type();
            $product->get_name();
            $product->get_slug();
            get_permalink($product->get_id());
            
            // Get Product Images
            $product->get_image_id();
            $product->get_image();
            $product->get_gallery_image_ids();
            
            // Get our new custom fields 
            $jacaudio_product_private_key_output = get_post_meta($jacaudio_product_prime, 'jacaudio_product_private_key', true);
            $jacaudio_product_active_amount      = get_post_meta($jacaudio_product_prime, 'jacaudio_product_registration_amount', true);
			      $jacaudio_product_name      = get_post_meta($jacaudio_product_prime, 'jacaudio_product_app_name', true);
            //$jacaudio_product_private_key_output . '<br />';
                   
			// Get user id
			$unsers_id_value = get_current_user_id();
			

	
			
//////////////////////////////////////////////////////////////////////////////////////			
// Check the plugin database for amount of used or unsed keys
//
//////////////////////////////////////////////////////////////////////////////////////	
		
			 //Table name to search
			 $table_name = $wpdb->prefix . 'jacaudio';
			 
			 // product query against users id
			 $query = $wpdb->get_results("
             SELECT * 
             FROM $table_name 
             WHERE user_id =  $unsers_id_value  
             AND application_id =  '$jacaudio_product_name'
             ", ARRAY_A);

			 

			 // amount of keys found in the database to check for amount you can have
			 $rowcount = $wpdb->num_rows;
			 
			 
			 //Echo our reselts if we have them
			 
			 if($rowcount != 0)
			 {
				 //Get the amount of total keys you can have
        $percentage = ($rowcount / $jacaudio_product_active_amount) * 100;


			  

	
	
?>
<div class="container">

    <div class="row">
        <div class="col-sm-2">
            <div class="rounded">
                <a href="<?php  echo get_permalink($product->get_id()); ?>"><?php echo $product->get_image();?></a>
            </div>
        </div>

        <div class="col-sm-10">
            <h3><a href="<?php  echo get_permalink($product->get_id()); ?>"><?php echo $product->get_name();?></a></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <br />
            <p style="font-size: 11px; margin-bottom: 0px;"><?php _e( 'You have used ', 'jac-audio-default' ); ?>
                <?php  echo $rowcount; ?> <?php _e( ' of your ', 'jac-audio-default' ); ?>
                <?php  echo $jacaudio_product_active_amount; ?> <?php _e( ' Keys.', 'jac-audio-default' ); ?></p>

            <div class="progress" style="height:10px;">
                <div class="progress-bar bg-secondary" style="width:<?php  echo $percentage; ?>%; height:10px"></div>
            </div>
            <br />

        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">

            <button type="button" class="btn btn-primary btn-block" data-toggle="collapse"
                data-target="#product<?php echo $product->get_id(); ?>"><?php _e( 'Show Authorised Devices', 'jac-audio-default' ); ?></button>


            <div id="product<?php echo $product->get_id(); ?>" class="collapse">

                <div class="container">

                    <br />
                    <p><?php _e( 'Current active devices:', 'jac-audio-default' ); ?></p>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?php _e( 'Activated', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Machine ID', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Mode', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Activation', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Active', 'jac-audio-default' ); ?></th>
                            </tr>
                        </thead>
                        <tbody>





                            <?php				
	

				 foreach($query as $row)
         {
				  
				 // convert the date to uk format
			     $timestamp = strtotime(str_replace('/', '.', $row["activation_date"]));
                 $mysql_date = date('d/m/Y', $timestamp); // 2011-04-03
				
?>

                            <tr id="<?php echo $row['id']; ?>">

                                <td><?php echo $mysql_date; ?></td>
                                <td><?php echo $row["machine_id"]; ?></td>
                                <td><?php echo $row["activation_method"]; ?></td>
                                <td><button type="button" id="deauther_button_<?php echo $row['id']; ?>"
                                        class="jac-deauth btn btn-outline-danger btn-sm" data-toggle="modal"
                                        data-target="#myModalDe"><?php _e( 'Deauthorise', 'jac-audio-default' ); ?></button>
                                </td>
                                <td><button type="button" class="btn btn-success btn-sm" title="Serial"
                                        <?php if($row["activation_method"] == 'Offline'){ ?>data-toggle="popover"
                                        data-placement="top" <?php } ?>
                                        data-content="<?php if($row["activation_method"] == 'Offline'){ echo $row["serial_number"];} ?>"><?php if($row["activation_method"] == 'Offline'){ _e( 'Key', 'jac-audio-default' ); }else {  _e( 'Yes', 'jac-audio-default' );  } ?></button>
                                </td>

                            </tr>


                            <?php				  
				  
				  
                 }
				 

				 // Work out keys extra 
				 $amount_of_key_left = $jacaudio_product_active_amount - $rowcount;

				 // If we have spare keys make table
				 if($amount_of_key_left >= 1)
				 {
					 // we have keys empty
				   for ($x = 1; $x <= $amount_of_key_left; $x++) 
				   { 
?> <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                                        data-target="#aoModal"
                                        data-app="<?php echo $jacaudio_product_name; ?>"><?php _e( 'Authorise offline', 'jac-audio-default' ); ?></button>
                                </td>
                                <td><button type="button"
                                        class="btn btn-outline-danger btn-sm"><?php _e( 'No', 'jac-audio-default' ); ?></button>
                                </td>
                            </tr>
                            <?php					 
				   }
					 
				 }
				 
				 
?>




                        </tbody>
                    </table>
                </div>


                <br />

            </div>
        </div>

    </div>






    <div class="clearfix"><br /></div>
    <hr />
</div>
<?php				
				
			 }
			 else
			 {
				 //This displays our autivations amount tables as we not registered anything
				 
				  //Get the amount of total keys you can have adding zero with no results
          $percentage = (0 / $jacaudio_product_active_amount) * 100;
				   
				   
?>

<div class="container">



    <div class="row">
        <div class="col-sm-2">
            <div class="rounded"><a href="<?php  echo get_permalink($product->get_id()); ?>">

                    <?php echo $product->get_image();?>
                </a>
            </div>
        </div>

        <div class="col-sm-10">
            <h3><a href="<?php  echo get_permalink($product->get_id()); ?>"><?php echo $product->get_name();?></a></h3>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <br />
            <p style="font-size: 11px; margin-bottom: 0px;">
                <?php _e( 'You have used ', 'jac-audio-default' ); ?><?php  echo $rowcount; ?><?php _e( ' of your ', 'jac-audio-default' ); ?>
                <?php  echo $jacaudio_product_active_amount; ?> <?php _e( ' keys.', 'jac-audio-default' ); ?></p>

            <div class="progress" style="height:10px;">
                <div class="progress-bar bg-secondary" style="width:<?php  echo $percentage; ?>%; height:10px"></div>
            </div>
            <br />

        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">

            <button type="button" class="btn btn-primary btn-block" data-toggle="collapse"
                data-target="#product<?php echo $product->get_id(); ?>"><?php _e( 'Show Authorised Devices', 'jac-audio-default' ); ?></button>


            <div id="product<?php echo $product->get_id(); ?>" class="collapse">

                <div class="container">

                    <br />
                    <p><?php _e( 'Current active devices:', 'jac-audio-default' ); ?></p>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?php _e( 'Activated', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Machine ID', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Mode', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Activation', 'jac-audio-default' ); ?></th>
                                <th><?php _e( 'Active', 'jac-audio-default' ); ?></th>
                            </tr>
                        </thead>
                        <tbody>






                            <?php
				   
				   for ($x = 1; $x <= $jacaudio_product_active_amount; $x++) 
				   {
					   
?>

                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                                        data-target="#aoModal"
                                        data-app="<?php echo $jacaudio_product_name; ?>"><?php _e( 'Authorise offline', 'jac-audio-default' ); ?></button>
                                </td>
                                <td><button type="button"
                                        class="btn btn-outline-danger btn-sm"><?php _e( 'No', 'jac-audio-default' ); ?></button>
                                </td>
                            </tr>

                            <?php					   
					   
                   } 
				   
?>



                        </tbody>
                    </table>
                </div>


                <br />

            </div>
        </div>

    </div>



    <div class="clearfix"><br /></div>
    <hr />







</div>


<?php				 
				 
			 }
			        
            
        }
        
        
    }
    
 ?>
<!-- The Modal deauth -->
<div class="modal fade" id="myModalDe">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">
                    <?php _e( 'Are you sure you want to deautherise this plugin?', 'jac-audio-default' ); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <form id="jac-deauth-form">

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" id="" class="jac-confirm-deauth btn btn-primary"
                                onclick="#"><?php _e( 'Deautherise', 'jac-audio-default' ); ?></button>
                            <p id="jac-status"></p>
                        </div>
                    </div>

                </form>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                    data-dismiss="modal"><?php _e( 'Close', 'jac-audio-default' ); ?></button>
            </div>

        </div>
    </div>
</div>

<!-- Modal end -->




<!-- The Modal -->
<div class="modal fade" id="aoModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title"><?php _e( 'Enter Machine ID and your Password', 'jac-audio-default' ); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <form id="jac-create" autocomplete="off">

                    <div class="form-group">
                        <input type="text" class="form-control" id="machine_auth"
                            placeholder="<?php _e( 'Machine ID', 'jac-audio-default' ); ?>" autocomplete="new-password">
                        <br />
                        <input type="password" class="form-control" id="password_auth"
                            placeholder="<?php _e( 'Password', 'jac-audio-default' ); ?>" autocomplete="new-password">
                        <input type="hidden" id="applicationname" name="applicationname" value="">
                        <p id="jac-status-ao"></p>

                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" id="jac-confirm-ao"
                                class="btn btn-primary"><?php _e( 'Register', 'jac-audio-default' ); ?></button>
                        </div>
                    </div>

                </form>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                    data-dismiss="modal"><?php _e( 'Close', 'jac-audio-default' ); ?></button>
            </div>

        </div>
    </div>
</div>

<!-- Modal end -->




<?php    
}

add_shortcode('jacaudio_product_short', 'jacaudio_product_shortcode');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Master admin key remover
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////




function jacaudio_product_master_remove()
{

//////////////
// Pull all data from the database and include unath button
// Add search user via javascript for searching
//////////////

if(is_admin() && is_user_logged_in())
{
	
	
	         global $wpdb;
	         // Table name to search
			 $table_name = $wpdb->prefix . 'jacaudio';
			 // product query against users id
			 $query = $wpdb->get_results("
             SELECT * 
             FROM $table_name 
             ", ARRAY_A);
			 // amount of keys found in the database to check for amount you can have
			 $rowcount = $wpdb->num_rows;
	 

			 // Echo our reselts if we have them
			 
			 if($rowcount != 0)
			 {
				 
?>
<br />
<div class="container">
    <input class="form-control" id="searchitems" type="text"
        placeholder="<?php _e( 'Search..', 'jac-audio-default' ); ?>">
    <br>

    <table id="master-key-delete" class="table table-hover">
        <thead>
            <tr>
                <th><?php _e( 'User Email', 'jac-audio-default' ); ?></th>
                <th><?php _e( 'User ID', 'jac-audio-default' ); ?></th>
                <th><?php _e( 'Application Name', 'jac-audio-default' ); ?></th>
                <th><?php _e( 'Activated', 'jac-audio-default' ); ?></th>
                <th><?php _e( 'Machine ID', 'jac-audio-default' ); ?></th>
                <th><?php _e( 'Mode', 'jac-audio-default' ); ?></th>
                <th><?php _e( 'Delete Key', 'jac-audio-default' ); ?></th>
            </tr>
        </thead>
        <tbody>

            <?php			 
						 
				 
				foreach($query as $row)
                {
					$user = get_user_by( 'id', $row["user_id"] );
					if ( ! empty( $user ) ) 
					{					
?>
            <tr id="">
                <td><?php echo $user->user_email; ?></td>
                <td><?php echo $row["user_id"]; ?></td>
                <td><?php echo $row["application_id"]; ?></td>
                <td><?php echo $row["activation_date"]; ?></td>
                <td><?php echo $row["machine_id"]; ?></td>
                <td><?php echo $row["activation_method"]; ?></td>
                <td><button type="button" id="master_deauther_button_<?php echo $row['id']; ?>"
                        class="master_deauther_button_panel btn btn-outline-danger btn-sm" data-toggle="modal"
                        data-target="#masterModalDe"><?php _e( 'Deauthorise', 'jac-audio-default' ); ?></button></td>
            </tr>
            <?php	
				   }
  
				}	 
?>

        </tbody>
    </table>
</div>
<?php	
				 
				 
			 }
			 else{
				 _e( 'There is no data in the applications database.', 'jac-audio-default' ); 
			 }

	
	
}else{
	_e( 'Only admins can view this page.', 'jac-audio-default' ); 
}




}

add_shortcode('jac-remove-keys-admin', 'jacaudio_product_master_remove');







////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajax master deauth file from panel
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// The function that handles the AJAX request
function jac_deathaction_master_callback() {

  check_ajax_referer( 'my-special-string', 'security' );
  $authinfo = intval( $_POST['authinfo'] );

 if ( is_user_logged_in() && is_admin() ) {

      // Delete the key
      global $wpdb;
      $table_name = $wpdb->prefix . 'jacaudio';    
      $wpdb->delete( $table_name, array( 'id' => $authinfo ) );
      
	   $authinfo = 'Deautherised plugin';
       echo $authinfo;
       die();

 }
 else{

    $authinfo = "An error occured please try again later!";
    echo $authinfo;
    die(); // this is required to return a proper result
 }

}
add_action( 'wp_ajax_jac_deathaction_master', 'jac_deathaction_master_callback' );

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajax deauth file from panel
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////




// The function that handles the AJAX request
function jac_deathaction_callback() {

  check_ajax_referer( 'my-special-string', 'security' );
  $authinfo = intval( $_POST['authinfo'] );

  $current_user = wp_get_current_user();
  $current_user_id = $current_user->ID;



 if ( is_user_logged_in() ) {

  
     // Table name to search
    global $wpdb;
	$table_name = $wpdb->prefix . 'jacaudio';    

  	 // product query against users id
			 $query = $wpdb->get_results("
             SELECT * 
             FROM  $table_name
             WHERE  id = $authinfo
             AND user_id =  $current_user_id
             ");   

       $rowcount = $wpdb->num_rows;

			 if($rowcount != 0)
       {
         
          $wpdb->delete( $table_name, array( 'id' => $authinfo ) );
		 
          $authinfo = 'Deautherised plugin';
          echo $authinfo;
          die();

       }
       else{
          $authinfo = 'Nothing found';
          echo $authinfo;
          die();
       }


 }
 else{

    $authinfo = "An error occured please try again later!";
    echo $authinfo;
    die(); // this is required to return a proper result
 }

 
  
}
add_action( 'wp_ajax_jac_deathaction', 'jac_deathaction_callback' );




////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajax autherise application for offline with machine id from panel
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function jac_athaction_callback() {
	

  check_ajax_referer( 'my-special-string', 'security' );
  $user_pw         = $_POST['password_a'];
  $product_mach    = $_POST['machine_a'];
  $product_post    = $_POST['application_a'];
  
                        
    
    if (is_user_logged_in()) 
	{
		
        $current_user     = wp_get_current_user();
        $user_id          = $current_user->ID;
        $user             = $current_user->user_pass;
		$gb_grab          = $current_user->user_email;
        $password_checker = wp_check_password($user_pw, $user, $user_id);
		
		
        
        if ($password_checker == 1) 
		{
            
			
			
			//Check the person owns the product
        global $wpdb;
        
        // This SQL query allows to get all the products purchased by the current user
        // In this example we sort products by date and product type
        $purchased_products_ids = $wpdb->get_col($wpdb->prepare("
        SELECT      itemmeta.meta_value
        FROM        " . $wpdb->prefix . "woocommerce_order_itemmeta itemmeta
        INNER JOIN  " . $wpdb->prefix . "woocommerce_order_items items
                    ON itemmeta.order_item_id = items.order_item_id
        INNER JOIN  $wpdb->posts orders
                    ON orders.ID = items.order_id
        INNER JOIN  $wpdb->postmeta ordermeta
                    ON orders.ID = ordermeta.post_id
        WHERE       itemmeta.meta_key = '_product_id'
                    AND ordermeta.meta_key = '_customer_user'
                    AND ordermeta.meta_value = %s
        ORDER BY    orders.post_date DESC
        ", $user_id));
        
        // Some orders may contain the same product, but we do not need it twice
        $purchased_products_ids = array_unique($purchased_products_ids);
        
        // If the customer purchased something
        if (!empty($purchased_products_ids)):
        // it is time for a regular WP_Query
            $purchased_products = new WP_Query(array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'post__in' => $purchased_products_ids,
                'orderby' => 'post__in',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_type',
                        'field' => 'slug',
                        'terms' => 'jac_audio'
                    )
                )
            ));
        //Grab our owned products from the jac_audio 
            $jac_audio_array    = array();
            while ($purchased_products->have_posts()):
                $purchased_products->the_post();
                $jac_audio_array[] = get_the_ID();
            endwhile;
            wp_reset_postdata();
        else:
        
        // respond to plugin
 
	   
        endif;
        
        
        
        // Empty array dont do anything as we outputted our no purchase already    
        if (!empty($jac_audio_array)) 
		{
          

            foreach ($jac_audio_array as $jacaudio_product_prime) 
			{
                
                // Grab our product ID
                $product = wc_get_product($jacaudio_product_prime);
                
                // Get Product General Info
                $product->get_type();
                $product->get_name();
                $product->get_slug();
                get_permalink($product->get_id());
                
                // Get our new custom fields 
                $jacaudio_product_private_key_output = get_post_meta($jacaudio_product_prime, 'jacaudio_product_private_key', true);
                $jacaudio_product_public_key_output  = get_post_meta($jacaudio_product_prime, 'jacaudio_product_public_key', true);
                $jacaudio_product_active_amount      = get_post_meta($jacaudio_product_prime, 'jacaudio_product_registration_amount', true);
                $jacaudio_product_name               = get_post_meta($jacaudio_product_prime, 'jacaudio_product_app_name', true);
				
				
                
                if ($product_post == $jacaudio_product_name) 
				{
					
					
                    
                    //////////////////////////////////////////////////////////////////////////////////////            
                    // Check the plugin database for amount of used or unsed keys
                    //////////////////////////////////////////////////////////////////////////////////////    
                    
                    //Table name to search
                    $table_name = $wpdb->prefix . 'jacaudio';
                    
                    // product query against users id
                    $query = $wpdb->get_results("
                    SELECT * 
                    FROM $table_name 
                    WHERE user_id =   $user_id 
                    AND application_id =  '$jacaudio_product_name'
                    ", ARRAY_A);
                    
                    // amount of keys found in the database to check for amount you can have
                    $rowcount = $wpdb->num_rows;
                    
                    //Echo our reselts if we have them
                    
                    if ($rowcount < $jacaudio_product_active_amount) 
					         {                      
                           //Send responce to plugin
						 
                         unlocker_sendResponse($jacaudio_product_name, $user_id, $gb_grab, $user_pw, $product_mach, $jacaudio_product_private_key_output, $jacaudio_product_public_key_output);
                                          
                    }
                    else
                    {
						
                         // respond to the plugin
			                   $authinfo = 'error products';
                         echo $authinfo;
                         die();

                        
                    }
                    
                    
                    
                }
                
                
            }
            
            
        }
			
			 
			
			
		}
		else {

    
               // respond to the plugin
			        $authinfo = 'error password';
               echo $authinfo;
               die();

        }
		
	}
  
 
}
add_action( 'wp_ajax_jac_athaction', 'jac_athaction_callback' );



function unlocker_sendResponse($jacaudio_product_name, $user_id, $gb_grab, $user_pw, $product_mach, $jacaudio_product_private_key_output, $jacaudio_product_public_key_output)
{
	
    $output; //run unix file and attach the key generated into the table                  
    $return_var; //will hold the number of lines in output string after execution
	$unlocker_url = dirname(__FILE__).'/auth/Unlocker';

     exec(escapeshellarg($unlocker_url).' '
         .escapeshellarg($jacaudio_product_name).' '
         .escapeshellarg($gb_grab).' '
          .escapeshellarg($user_pw).' '
         .escapeshellarg($product_mach).' '
         .escapeshellarg($jacaudio_product_private_key_output.$jacaudio_product_public_key_output) //Keys private -public     
         , $output, $return_var);
    
    
    $formattedOutput;
    
    for ($i = 5; $i < $return_var; $i++)
    {
        $formattedOutput .= $output[$i];
        
        if ($i <= 4) //everything before the key
        {
            $formattedOutput .= "\n";
        }
    }
  
                        // insert to database
                        $the_date = date('Y-m-d');
                        
                        global $wpdb;
                        $table_name = $wpdb->prefix . 'jacaudio';
                        $data       = array(
                            'user_id' => $user_id,
                            'application_id' => $jacaudio_product_name,
                            'activation_date' => $the_date,
                            'machine_id' => $product_mach,
                            'activation_method' => 'Offline',
                            'serial_number' => $formattedOutput
                        );
                        // $format = array('%s','%d');
                        $wpdb->insert($table_name, $data);
                        // $my_id = $wpdb->insert_id;



                         //Send responce to plugin
						             $authinfo = 'offline actioned';
						             echo $authinfo;
                         die();
						 						 
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WooCommerce is not installed or active WARNING 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////



}else{


add_action( 'admin_notices', function() 
{ 
    
?>

<div class="notice notice-error">
    <p><strong>Notice!</strong><?php _e( 'JUCE audio keys requires WooCommerce to be installed and active to work.', 'jac-audio-default' ); ?>
    </p>
</div>

<?php 
} );


}